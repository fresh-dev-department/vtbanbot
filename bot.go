package main

import (
	"log"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

// CommandHandler функция для обработки комманды.
type CommandHandler func(message *tgbotapi.Message, bot *tgbotapi.BotAPI,
	vtbot *VoteBot)

// CallbackQueryHandler функция для обработки нажатия на кнопку.
type CallbackQueryHandler func(callbackQuery *tgbotapi.CallbackQuery,
	bot *tgbotapi.BotAPI, vtbot *VoteBot)

var (
	commandHandlers = map[string]CommandHandler{
		"help":     help,
		"voteban":  voteban,
		"vtban":    voteban,
		"vote":     vote,
		"getvotes": getvotes,
	}

	callbackQueryHandlers = map[string]CallbackQueryHandler{
		"voteup":   inlineVoteUp,
		"votedown": inlineVoteDown,
	}
)

var (
	currVoting        = NewVoting(60 * time.Second)
	currVotingResults = NewVotingResults()
)

// NewVoteBot создает новый экземпляр бота.
func NewVoteBot(name string, settings *BotSettings) *VoteBot {
	return &VoteBot{name, settings}
}

// Run запускает главный цикл бота.
func (vtbot *VoteBot) Run() {
	tgbot, err := tgbotapi.NewBotAPI(vtbot.GetToken())
	if err != nil {
		log.Panic(err)
	}

	tgbot.Debug = true

	log.Printf("Authorized on account %s", tgbot.Self.UserName)

	updateConfig := tgbotapi.NewUpdate(0)
	updateConfig.Timeout = 60

	updates, err := tgbot.GetUpdatesChan(updateConfig)

	// Очистка обновлений от старых сообщений.
	time.Sleep(time.Millisecond * 500)
	updates.Clear()

	for update := range updates {
		if update.CallbackQuery != nil {
			if update.CallbackQuery.Message == nil {
				continue
			}

			queryText := update.CallbackQuery.Message.Text
			if handler, ok := callbackQueryHandlers[queryText]; ok {
				handler(update.CallbackQuery, tgbot, vtbot)
			} else {
				badQuery(update.CallbackQuery, tgbot, vtbot)
			}
		}

		if update.Message != nil {
			if !update.Message.IsCommand() {
				continue
			}

			command := update.Message.Command()
			if handler, ok := commandHandlers[command]; ok {
				handler(update.Message, tgbot, vtbot)
			} else {
				badCommand(update.Message, tgbot, vtbot)
			}
		}
	}
}

func help(message *tgbotapi.Message, bot *tgbotapi.BotAPI, vtbot *VoteBot) {
	if vtbot.IsAdmin(message.From.ID) {

	}
}

func voteban(message *tgbotapi.Message, bot *tgbotapi.BotAPI, vtbot *VoteBot) {
	if currVoting.IsActive {
		return
	}

	currVoting = NewVoting(60 * time.Second)
	currVoting.IsActive = true

	// Т.к. возможно получение только последних полных результатов
	// голосования, мы не сохраняем их копию во время начала нового.
	currVotingResults = NewVotingResults()

	upVoteButton := tgbotapi.NewInlineKeyboardButtonData("👍", "voteup")
	downVoteButton := tgbotapi.NewInlineKeyboardButtonData("👎", "votedown")
	buttonRow := tgbotapi.NewInlineKeyboardRow(upVoteButton, downVoteButton)
	keyboardMarkup := tgbotapi.NewInlineKeyboardMarkup(buttonRow)

	// Нет поддержки удобной отправки сообщения сразу с Inline Keyboard,
	// поэтому используется неочевидный метод с изменением сообщения, после
	// его отправки и назначения ему кнопок.
	voteMessageConfig := tgbotapi.NewMessage(vtbot.GetMainChatID(),
		vtbot.GetVoteText())
	voteMessage, err := bot.Send(voteMessageConfig)
	if err != nil {
		log.Printf("Error happend during sending the message: %s", err)
		return
	}

	editVoteMessageConfig := tgbotapi.NewEditMessageReplyMarkup(
		vtbot.GetMainChatID(), voteMessage.MessageID, keyboardMarkup)
	bot.Send(editVoteMessageConfig)

	bot.PinChatMessage(tgbotapi.PinChatMessageConfig{
		vtbot.GetMainChatID(),
		voteMessage.MessageID,
		false,
	})

	currVoting.Run()
}

func vote(message *tgbotapi.Message, bot *tgbotapi.BotAPI, vtbot *VoteBot) {
	if !currVoting.IsActive {
		return
	}
}

func getvotes(message *tgbotapi.Message, bot *tgbotapi.BotAPI, vtbot *VoteBot) {

}

func badCommand(message *tgbotapi.Message, bot *tgbotapi.BotAPI, vtbot *VoteBot) {

}

func inlineVoteUp(callbackQuery *tgbotapi.CallbackQuery, bot *tgbotapi.BotAPI, vtbot *VoteBot) {
	if !currVoting.IsActive {
		return
	}

	currVotingResults.AddVote(Vote{isUp, true, callbackQuery.From})
}

func inlineVoteDown(callbackQuery *tgbotapi.CallbackQuery, bot *tgbotapi.BotAPI, vtbot *VoteBot) {
	if !currVoting.IsActive {
		return
	}
}

func badQuery(callbackQuery *tgbotapi.CallbackQuery, bot *tgbotapi.BotAPI, vtbot *VoteBot) {

}
