package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"
)

// NewBotSettings создает новый экземпляр настроек бота.
func NewBotSettings() *BotSettings {
	return &BotSettings{"", 0, 0,
		UserIDList{}, UserIDList{}, UserIDList{}, ""}
}

// Load загружает настройки из файла.
func (settings *BotSettings) Load(filename string) error {
	data, err := ioutil.ReadFile("script.json")
	if err != nil {
		fmt.Printf("<<<READING ERROR>>>: %s\n", err)
		return err
	}

	err = json.Unmarshal(data, settings)
	if err != nil {
		fmt.Printf("<<<JSON ENCODING ERROR>>>: %s\n", err)
		return err
	}

	return nil
}

// Save сохраняет настройки в файл.
func (settings *BotSettings) Save(filename string) error {
	data, err := json.Marshal(settings)
	if err != nil {
		fmt.Printf("<<<JSON DECODING ERROR>>>: %s\n", err)
		return err
	}

	var fm os.FileMode
	if runtime.GOOS == "windows" {
		fm = 0644
	} else {
		fm = os.ModePerm
	}

	err = ioutil.WriteFile(filename, data, fm)
	if err != nil {
		fmt.Printf("<<<WRITING ERROR>>>: %s\n", err)
		return err
	}

	return nil
}
