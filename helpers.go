package main

import "reflect"

// Contains ...
func Contains(list interface{}, item interface{}) bool {
	listValue := reflect.ValueOf(list)
	if listValue.Kind() == reflect.Slice {
		for i := 0; i < listValue.Len(); i++ {
			if listValue.Index(i).Interface() == item {
				return true
			}
		}
	}
	return false
}

// Find ...
func Find(list interface{}, item interface{}) int {
	listValue := reflect.ValueOf(list)
	if listValue.Kind() == reflect.Slice {
		for i := 0; i < listValue.Len(); i++ {
			if listValue.Index(i).Interface() == item {
				return i
			}
		}
	}
	return -1
}
