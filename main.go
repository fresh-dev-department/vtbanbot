package main

const SettingsFilename = "data/config.json"

func main() {
	botSettings := NewBotSettings()
	botSettings.Load(SettingsFilename)
	defer botSettings.Save(SettingsFilename)
	vb := NewVoteBot("Ufologists VoteBan bot", botSettings)
	vb.Run()
}
