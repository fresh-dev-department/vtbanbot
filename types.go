package main

import "errors"

// UserIDList типозаменитель для слайсов строк
type UserIDList []Item

// BotSettings структура для хранения настроек бота.
type BotSettings struct {
	Token string `json:"token"`

	MainChatID  int64 `json:"main_chat"`
	AdminChatID int64 `json:"admin_chat"`

	BlackList UserIDList `json:"blacklist"`
	WhiteList UserIDList `json:"whitelist"`
	AdminList UserIDList `json:"adminlist"`

	VoteText string `json:"vote_text"`
}

// Append добавляет id в список.
func (list *UserIDList) Append(id int) error {
	if Contains(*list, id) {
		return errors.New("Item already contains in list.")
	}
	*list = append(*list, id)
	return nil
}

// Delete удаляет id из списка.
func (list *UserIDList) Delete(id int) error {
	if Contains(*list, id) {
		pos := Find(*list, id)
		if pos >= 0 {
			*list = append((*list)[:pos], (*list)[pos+1:]...)
			return nil
		}
	}
	return errors.New("Item isn't in list yet!")
}

// VoteBot структура для удобного взаимодействия с настройками бота.
type VoteBot struct {
	name     string
	settings *BotSettings
}

// GetName возвращает имя бота.
func (bot *VoteBot) GetName() string {
	return bot.name
}

// GetToken возвращает токен.
func (bot *VoteBot) GetToken() string {
	return bot.settings.Token
}

// GetMainChatID возвращает id главного чата пользователей.
func (bot *VoteBot) GetMainChatID() int64 {
	return bot.settings.MainChatID
}

// GetAdminChatID возвращает id чата с администрацией.
func (bot *VoteBot) GetAdminChatID() int64 {
	return bot.settings.AdminChatID
}

// GetVoteText возвращает текст, который будет отображен в сообщении во время
// голосования.
func (bot *VoteBot) GetVoteText() string {
	return bot.settings.VoteText
}

// IsBlacklisted проверяет наличие пользователя в черном списке.
func (bot *VoteBot) IsBlacklisted(userid int) bool {
	return Contains(bot.settings.BlackList, userid)
}

// IsWhitelisted проверяет наличие пользователя в белом списке.
func (bot *VoteBot) IsWhitelisted(userid int) bool {
	return Contains(bot.settings.WhiteList, userid)
}

// IsAdmin проверяет наличие пользователя в списке администраторов.
func (bot *VoteBot) IsAdmin(userid int) bool {
	return Contains(bot.settings.AdminList, userid)
}

// AddToBlackList добавляет пользователя в черный список.
func (bot *VoteBot) AddToBlackList(userid int) error {
	return bot.settings.BlackList.Append(userid)
}

// RemoveFromBlackList удаляет пользователя из черного списка.
func (bot *VoteBot) RemoveFromBlackList(userid int) error {
	return bot.settings.BlackList.Delete(userid)
}

// AddToWhiteList добавляет пользователя в белый список.
func (bot *VoteBot) AddToWhiteList(userid int) error {
	return bot.settings.WhiteList.Append(userid)
}

// RemoveFromWhiteList удаляет пользователя из белого списка.
func (bot *VoteBot) RemoveFromWhiteList(userid int) error {
	return bot.settings.WhiteList.Delete(userid)
}

// AddToAdminList добавляет пользователя в список администраторов.
func (bot *VoteBot) AddToAdminList(userid int) error {
	return bot.settings.AdminList.Append(userid)
}

// RemoveFromAdminList удаляет пользователя из списка администраторов.
func (bot *VoteBot) RemoveFromAdminList(userid int) error {
	return bot.settings.AdminList.Delete(userid)
}
