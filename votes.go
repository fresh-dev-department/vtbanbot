package main

import (
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

// Voting структура для хранения состояния текущего голосования.
type Voting struct {
	Timeout  time.Duration
	IsActive bool
}

// Vote структура для хранения отдельного голоса человека.
type Vote struct {
	IsUp      bool
	IsPrivate bool
	User      *tgbotapi.User
}

// VotingResults структура для хранения результатов голосования.
type VotingResults struct {
	UpVoteCount   int
	DownVoteCount int
	Votes         []Vote
}

// NewVoting создает новое голосование.
func NewVoting(timeout time.Duration) *Voting {
	return &Voting{timeout, false}
}

// NewVotingResults создает новые результаты голосования.
func NewVotingResults() *VotingResults {
	return &VotingResults{0, 0, []Vote{}}
}

// AddVote добавляет новый голос к результатам учитывая его параметры.
func (results *VotingResults) AddVote(vote Vote) {
	Contains(results.Votes, vote)
	voteValue := 1
	if vote.IsPrivate {
		voteValue = 2
	}

	if vote.IsUp {
		results.UpVoteCount += voteValue
	} else {
		results.DownVoteCount += voteValue
	}

	results.Votes = append(results.Votes, vote)
}

func (results *VotingResults) DeleteVote(vote Vote) {

}

// Run запускает голосование с таймингом. После истечения времени состояние
// голосование становится инактивным.
func (voting *Voting) Run() {
	if voting.IsActive {
		timer := time.NewTimer(voting.Timeout)
		go func() {
			<-timer.C
			voting.IsActive = false
		}()
	}
}
